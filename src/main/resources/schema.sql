--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.11
-- Dumped by pg_dump version 9.6.11

-- Started on 2019-03-22 16:42:40 CET

--
-- TOC entry 1 (class 3079 OID 12393)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner:
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2137 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 185 (class 1259 OID 16387)
-- Name: users; Type: TABLE; Schema: public; Owner: smarthome
--

CREATE TABLE IF NOT EXISTS public.users ( id integer NOT NULL, username text NOT NULL, password text NOT NULL );


ALTER TABLE public.users OWNER TO smarthome;

--
-- TOC entry 186 (class 1259 OID 16413)
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: smarthome
--

CREATE SEQUENCE IF NOT EXISTS public.users_id_seq START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO smarthome;

--
-- TOC entry 2138 (class 0 OID 0)
-- Dependencies: 186
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: smarthome
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- TOC entry 2006 (class 2604 OID 16415)
-- Name: users id; Type: DEFAULT; Schema: public; Owner: smarthome
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- TOC entry 2128 (class 0 OID 16387)
-- Dependencies: 185
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: smarthome
--


--
-- TOC entry 2139 (class 0 OID 0)
-- Dependencies: 186
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: smarthome
--

SELECT pg_catalog.setval('public.users_id_seq', 2, true);


--
-- TOC entry 2008 (class 2606 OID 16417)
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: smarthome
--

ALTER TABLE public.users DROP CONSTRAINT IF EXISTS users_pkey;
ALTER TABLE public.users ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- TOC entry 2010 (class 2606 OID 16428)
-- Name: users users_username_key; Type: CONSTRAINT; Schema: public; Owner: smarthome
--

ALTER TABLE public.users DROP CONSTRAINT IF EXISTS users_username_key;
ALTER TABLE public.users ADD CONSTRAINT users_username_key UNIQUE (username);


-- Completed on 2019-03-22 16:42:40 CET

--
-- PostgreSQL database dump complete
--


CREATE TABLE IF NOT EXISTS public.devices ( id integer NOT NULL, uuid text NOT NULL, ip text NOT NULL, name text NOT NULL, active bool NOT NULL );


ALTER TABLE public.devices OWNER TO smarthome;

--
-- TOC entry 186 (class 1259 OID 16413)
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: smarthome
--

CREATE SEQUENCE IF NOT EXISTS public.devices_id_seq START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;


ALTER TABLE public.devices_id_seq OWNER TO smarthome;

--
-- TOC entry 2138 (class 0 OID 0)
-- Dependencies: 186
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: smarthome
--

ALTER SEQUENCE public.devices_id_seq OWNED BY public.devices.id;


--
-- TOC entry 2006 (class 2604 OID 16415)
-- Name: users id; Type: DEFAULT; Schema: public; Owner: smarthome
--

ALTER TABLE ONLY public.devices ALTER COLUMN id SET DEFAULT nextval('public.devices_id_seq'::regclass);


--
-- TOC entry 2128 (class 0 OID 16387)
-- Dependencies: 185
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: smarthome
--


--
-- TOC entry 2139 (class 0 OID 0)
-- Dependencies: 186
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: smarthome
--

SELECT pg_catalog.setval('public.devices_id_seq', 3, true);


--
-- TOC entry 2008 (class 2606 OID 16417)
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: smarthome
--

ALTER TABLE public.devices DROP CONSTRAINT IF EXISTS devices_pkey;
ALTER TABLE public.devices ADD CONSTRAINT devices_pkey PRIMARY KEY (id);


--
-- TOC entry 2010 (class 2606 OID 16428)
-- Name: users users_username_key; Type: CONSTRAINT; Schema: public; Owner: smarthome
--

ALTER TABLE public.devices DROP CONSTRAINT IF EXISTS devices_uuid_key;
ALTER TABLE public.devices ADD CONSTRAINT devices_uuid_key UNIQUE (uuid);
