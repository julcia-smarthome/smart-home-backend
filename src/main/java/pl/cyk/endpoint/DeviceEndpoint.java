package pl.cyk.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.cyk.data.Device;

import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping("/device")
public class DeviceEndpoint {

    @Autowired
    private DataSource dataSource;
    @Autowired
    private HttpServletRequest request;

    private String getClientIp() {
        String remoteAddr = "";
        if (request != null) {
            remoteAddr = request.getHeader("X-FORWARDED-FOR");
            if (remoteAddr == null || "".equals(remoteAddr))
                remoteAddr = request.getRemoteAddr();
        }
        return remoteAddr;
    }

    @RequestMapping(value="/list", method=GET)
    public List<Device> getDevices(){
        try(Connection connection = dataSource.getConnection();
            Statement statement=connection.createStatement();
            ResultSet resultSet=statement.executeQuery("SELECT id, uuid, ip, name, active FROM devices")
        ){
            List<Device> devices=new LinkedList<>();
            while (resultSet.next()){
                Device device=new Device()
                        .withId(resultSet.getInt("id"))
                        .withUUID(resultSet.getString("uuid"))
                        .withActive(resultSet.getBoolean("active"))
                        .withIpAddress(resultSet.getString("ip"))
                        .withActive(resultSet.getBoolean("active"));
                devices.add(device);
            }
            return devices;
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(value="/listactive", method=GET)
    public List<Device> getActiveDevices(){
        try(Connection connection = dataSource.getConnection();
            Statement statement=connection.createStatement();
            ResultSet resultSet=statement.executeQuery("SELECT id, uuid, ip, name, active FROM devices WHERE active=true")
        ){
            List<Device> devices=new LinkedList<>();
            while (resultSet.next()){
                Device device=new Device()
                        .withId(resultSet.getInt("id"))
                        .withUUID(resultSet.getString("uuid"))
                        .withActive(resultSet.getBoolean("active"))
                        .withIpAddress(resultSet.getString("ip"))
                        .withActive(resultSet.getBoolean("active"));
                devices.add(device);
            }
            return devices;
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(value="/add", method=POST)
    public Boolean registerDevice(@RequestBody Device device){
        try(Connection connection = dataSource.getConnection();
            PreparedStatement statement=connection.prepareStatement("INSERT INTO devices (uuid, name, ip, active) VALUES (?, ?, ?, false)  ON CONFLICT (uuid) DO NOTHING");
        ) {
            statement.setString(1, device.getUuid());
            statement.setString(2, device.getName());
            statement.setString(3, getClientIp());
            statement.executeUpdate();
            return true;
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        return false;
    }
}
