package pl.cyk.endpoint;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping("/user")
public class UserEndpoint {

    @Autowired
    private DataSource dataSource;
    @Autowired
    private HttpSession session;

    private Integer getUserId(){
        return (Integer)session.getAttribute("id");
    }

    @RequestMapping(value="/isloggedin", method=GET)
    public Boolean isUserLoggedIn(){
        Integer id=getUserId();
        return (id!=null && id>0);
    }

    @RequestMapping(value="/login", method=POST)
    public Boolean login(@RequestParam("username") String userName, @RequestParam("password") String password){
        try(Connection connection = dataSource.getConnection();
            PreparedStatement statement=connection.prepareStatement("SELECT id, username, password FROM users WHERE username=?")
        ){
            statement.setString(1,userName);
            try(ResultSet resultSet=statement.executeQuery()) {
                if (resultSet.next()){
                    String hash=resultSet.getString("password");
                    if(BCrypt.checkpw(password, hash)){
                        Integer id=resultSet.getInt("id");
                        session.setAttribute("id",id);
                        return true;
                    }
                }
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        return false;
    }

    @RequestMapping(value="/changepassword", method=POST)
    public Boolean changePassword(@RequestParam("password") String password){
        if(isUserLoggedIn()) {
            try (Connection connection = dataSource.getConnection();
                 PreparedStatement statement = connection.prepareStatement("UPDATE users SET password=? WHERE id=?")
            ) {
                String hash=BCrypt.hashpw(password,BCrypt.gensalt());
                statement.setString(1, hash);
                statement.setInt(2, getUserId());
                return statement.executeUpdate()==1;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return false;
    }
}
