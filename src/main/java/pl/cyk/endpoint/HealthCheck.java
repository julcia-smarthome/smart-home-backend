package pl.cyk.endpoint;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HealthCheck {

    @RequestMapping("/healthcheck")
    public static String healthCheck(){
        return "OK";
    }
}
