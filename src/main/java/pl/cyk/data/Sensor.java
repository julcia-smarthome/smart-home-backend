package pl.cyk.data;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public abstract class Sensor extends Device{
    private String units;
    private List<MeasurePair> values;

    private final double MAX_VALUE;
    private final double MIN_VALUE;

    private static final String NO_DESCRIPTION = "";

    private String description;

    public Sensor(String name, LocalDateTime initTime, double initValue,
                  String units, String description, double minValue, double maxValue) {
        super.withName(name);
        this.description = description;

        this.values = new ArrayList<>();

        this.units = units;

        MIN_VALUE = minValue;
        MAX_VALUE = maxValue;

        this.addValue(initTime, initValue);
    }

    public Sensor(String name, LocalDateTime initTime, String units) {
        this(name, initTime, 0, units, NO_DESCRIPTION, -Double.MAX_VALUE, Double.MAX_VALUE);
    }

    public Sensor(String name, LocalDateTime initTime, double initValue, String units) {
        this(name, initTime, initValue, units, NO_DESCRIPTION, -Double.MAX_VALUE, Double.MAX_VALUE);
    }

    public String getDescription() {
        return description;
    }

    public String getUnits() {
        return units;
    }

    public MeasurePair geLasttValue() {
        return values.get(values.size() - 1);
    }

    public void addValue(LocalDateTime time, double newValue) {
        if (newValue >= MIN_VALUE && newValue <= MAX_VALUE && time != null) {
            values.add(new MeasurePair(time, newValue));
        }
    }
}
