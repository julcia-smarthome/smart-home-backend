package pl.cyk.data;

import java.time.LocalDateTime;

public class MeasurePair {
    private LocalDateTime time;
    private double value;

    public MeasurePair (LocalDateTime time, double value) {
        this.time = time;
        this.value = value;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public double getValue() {
        return value;
    }
}
