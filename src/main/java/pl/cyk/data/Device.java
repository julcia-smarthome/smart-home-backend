package pl.cyk.data;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Device {
    private Integer id;
    private String uuid;
    private String name;
    private Boolean active;
    private String ipAddress;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public Device withId(Integer id){
        this.id=id;
        return this;
    }

    public Device withName(String name){
        this.name=name;
        return this;
    }

    public Device withUUID(String uuid){
        this.uuid=uuid;
        return this;
    }

    public Device withActive(Boolean active){
        this.active=active;
        return this;
    }

    public Device withIpAddress(String ipAddress){
        this.ipAddress=ipAddress;
        return this;
    }
}
