# Julcia Smart Home

## Compile
`mvn package`

## Run
`mvn embedded-postgresql:start exec:java` or `java -jar target/server-1.0.jar`
#### Connect
Now you can run `curl localhost:8080/healthcheck`
+ check you are logged in `curl localhost:8080/user/isloggedin`
+ login `curl --data "username=marcel&password=admin321" localhost:8080/user/login`

to save cookies add `-b cookie.txt -c cookie.txt` attributes